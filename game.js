const GAME_WIDTH = 800;
const GAME_HEIGTH = 600;
const clamp = (num, min, max) => Math.min(Math.max(num, min), max);


class Pew {
    constructor(source) {
        this.canvas = document.createElement('CANVAS');
        this.canvas.setAttribute("width", 2);
        this.canvas.setAttribute("height", 10);
        this.x = source.originX;
        this.y = source.originY;
        this.m = source.movement;

        const ctx = this.canvas.getContext('2d');
        ctx.fillStyle = 'white';
        ctx.fillRect(0, 0, 2, 10);

        this.move();

    }
    move() {
        this.y += this.m;
        setTimeout(() => this.move(), 100);
    }


    draw(ctx) {
        ctx.drawImage(this.canvas, this.x, this.y);
    }

}


class Ship {
    constructor() {
        this.loaded = false;
        this.image = new Image();
        this.x = 0;

        this.image.addEventListener('load', () => {
            this.loaded = true;
            this.x = GAME_WIDTH / 2 - this.image.width / 2;

        });
        this.image.src = 'assets/Ship.png';
        // this.x = GAME_WIDTH / 2;
        this.isMoving = false;
        this.speed = 0;
    }
    moveLeft() {
        this.speed = -16;
        if (!this.isMoving) {
            this.isMoving = true;
        }
        this.move();
    }

    moveRight() {
        this.speed = 16;
        if (!this.isMoving) {
            this.isMoving = true;
        }
        this.move();
    }

    move() {
        this.position = this.x + this.speed;
        if (this.isMoving) {
            setTimeout(() => this.move(), 100);
        }
    }

    stop() {
        this.isMoving = false;
    }




    set position(value) {
        const maxWidth = GAME_WIDTH - this.image.width;
        this.x = clamp(value, 0, maxWidth);
    }
    get originX() {
        return this.x + this.image.width / 2;
    }
    get originY() {
        return 550;
    }
    get movement() {
        return -20;
    }

    draw(ctx) {
        ctx.drawImage(this.image, this.x, GAME_HEIGTH - 50);
    }


}
class Invader {
    constructor(type, x, y) {
        this.x = x;
        this.y = y;
        this.imageIndex = 0;
        this.image = [new Image(), new Image()];
        this.image[0].src = `assets/Invader${type}1.png`;
        this.image[1].src = `assets/Invader${type}2.png`;
        this.swap();
    }
    contains(fire){
        const width = this.image[0].width;
        const height = this.image[0].height;
        return fire.x >= this.x && 
            fire.x + 1 <= this.x + width &&
            fire.y < this.y + height;

    }


    swap() {
        this.imageIndex = ++this.imageIndex % 2;
        setTimeout(() => this.swap(), 1000);
    }
    get originX() {
        return this.x + this.image.width / 2;
    }
    get originY() {
        return 550;
    }
    get movement() {
        return -8;
    }

    draw(ctx) {
        ctx.drawImage(this.image[this.imageIndex], this.x, this.y);
    }


}


class spaceInvaders {
    constructor() {
        this.ship = new Ship();
        this.invaders = [];
        this.fire = [];
        this.canvas = document.createElement('CANVAS');
        this.canvas.setAttribute("width", GAME_WIDTH);
        this.canvas.setAttribute("height", GAME_HEIGTH);
        //      this.canvas.addEventListener('mousemove' , e =>{
        //          this.ship.position = e.offsetX;
        //         this.draw();
        //  } )

        this.ctx = this.canvas.getContext('2d');
        for (let i = 0; i < 5; ++i) {
            this.invaders.push(new Invader('C',30 + i * 60, 30));
            this.invaders.push(new Invader('C',30 + i * 60, 80));
            this.invaders.push(new Invader('B',30 + i * 60, 130));
            this.invaders.push(new Invader('B',30 + i * 60, 180));
            this.invaders.push(new Invader('A',30 + i * 60, 230));
            this.invaders.push(new Invader('A',30 + i * 60, 280));
        }

    }


    draw() {
        this.ctx.fillRect(0, 0, GAME_WIDTH, GAME_HEIGTH);
        this.ship.draw(this.ctx);

        for(let i = this.invaders.length - 1; i >= 0; --i ){
            if( this.invaders[i].deleted) continue;
            for(let f = 0; f < this.fire.length; ++f) {
                if (!this.fire[f].deleted && this.invaders[i].contains(this.fire[f])){
                    this.fire.splice(f,1);
                    this.invaders.splice(i,1);
                    break;
                }
            }
        }

        this.fire.forEach(fire => !fire.deleted && fire.draw(this.ctx));
        this.invaders.forEach(invader => !invader.deleted && invader.draw(this.ctx));
        window.requestAnimationFrame(() => this.draw());
    }

    handleKey(event) {
        console.log(event.code);
        if (event.type === 'keydown') {
            switch (event.code) {
                case 'Space':
                    this.fire.push(new Pew(this.ship));
                    break;
                case 'ArrowLeft':
                    this.ship.moveLeft();
                    break;
                case 'ArrowRight':
                    this.ship.moveRight();
                    break;

            }
        }
        else if (event.code.includes('Arrow')) {
            this.ship.stop();
        }

    }

    init() {
        document.body.appendChild(this.canvas);
        this.draw();
    }
}


const game = new spaceInvaders();


window.addEventListener('keydown', event => !event.repeat && game.handleKey(event));
window.addEventListener('keyup', event => !event.repeat && game.handleKey(event));
window.addEventListener('DOMContentLoaded', () => game.init());